package com.amartha.smsserver.WebSocketServer;


import android.app.PendingIntent;
import android.telephony.SmsManager;
import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class SmsGatewayServer extends WebSocketServer{
    /*
       {
           "no"  : "081214628696",
           "msg" : "hello world!"
       }
    */
    /*
    * curl -i -N -H "Connection: Upgrade" -H "Upgrade: websocket" -H "Host: 10.0.1.13:8080" -H "Origin:http://10.0.1.13:8080" http://10.0.1.13:8080 */
    /* curl -H "Content-Type: application/json" -X POST -d '{"no":"081214628696","msg":"Ini adalah tes."}' http://10.0.1.13:8080 */
    private PendingIntent sendIntent;
    private PendingIntent deliveryIntent;

    public SmsGatewayServer(int port) throws UnknownHostException{
        super(new InetSocketAddress(port));
    }

    public void setSendIntent(PendingIntent sendIntent){
        this.sendIntent = sendIntent;
    }

    public void setDeliveryIntent(PendingIntent deliveryIntent){
        this.deliveryIntent = deliveryIntent;
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        //Add Client(s) to Container
        SmsGatewayContainer.add(conn);
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        //Remove Client(s) from Container
        SmsGatewayContainer.remove(conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        try{
            JSONObject jsonObject = new JSONObject(message);
            //Extract No & Pesan from JSON Object
            String txtNum  = jsonObject.getString("no");
            String txtMsg  = jsonObject.getString("msg");
            //Send SMS to Designated No.
            SmsManager.getDefault().sendTextMessage(txtNum, null, txtMsg, sendIntent, deliveryIntent);
            Log.i(SmsGatewayServer.class.getName(), "Success sending SMS to " + txtNum);
        }
        catch(JSONException jex){
            conn.send("Format JSON Salah.");
            Log.e(SmsGatewayServer.class.getName(), "Error sending SMS");
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {

    }
}
