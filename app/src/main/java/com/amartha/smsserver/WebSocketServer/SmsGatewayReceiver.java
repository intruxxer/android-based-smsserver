package com.amartha.smsserver.WebSocketServer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsGatewayReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        if(bundle != null)
        {
            //Receiving incoming SMS
            Object[] pdus = (Object[]) bundle.get("pdus");
            for(Object pdu : pdus)
            {
                //Convert PDUs received by Broadcast Receiver into SMS Message
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[])pdu);
                String     smsHeader  = smsMessage.getOriginatingAddress();
                String     smsBody    = smsMessage.getMessageBody();
                String     smsLog     = "SMS: (" + smsHeader + ") " + smsBody;
                //Send back to Client
                SmsGatewayContainer.send(smsLog);
                //Logcat to Screen
                Log.i("Incoming SMS", "From: " + smsHeader + " (" + smsBody + ")");
            }
        }
    }
}
