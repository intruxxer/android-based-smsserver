package com.amartha.smsserver.WebSocketServer;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.amartha.smsserver.R;

import java.io.IOException;
import java.net.UnknownHostException;

public class SMSServer extends AppCompatActivity {
    private final int portNo = 8080;
    private SmsGatewayServer smsServer;

    //UI References
    private TextView statusTxt;
    private Button   startBtn;
    private Button   stopBtn;
    public SMSServer(){
        try { smsServer = new SmsGatewayServer(portNo); }
        catch(UnknownHostException e) { Log.i(SMSServer.class.getName(), e.getMessage(), e); }
    }

    //Starting SMS Gateway Server
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.smslist);
        //Init UI in Main Window
        statusTxt = (TextView) findViewById(R.id.txtStatusServer);
        startBtn  = (Button)   findViewById(R.id.btnStartServer);
        stopBtn   = (Button)   findViewById(R.id.btnStopServer);

        //Set Receiver for Each Intent:
        // (1) sendIntent()
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch(getResultCode()){
                    case Activity.RESULT_OK:
                        SmsGatewayContainer.send("SMS succesfully sent.");
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        SmsGatewayContainer.send("Generic Failure.");
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        SmsGatewayContainer.send("No Service.");
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        SmsGatewayContainer.send("PDU(S) is/are NULLified.");
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        SmsGatewayContainer.send("Radio Off. Check Data/Communication..");
                        break;
                    default:
                        break;
                }
            }
        }, new IntentFilter("SMS_SENT"));

        // (2) deliveryIntent()
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch(getResultCode()){
                    case Activity.RESULT_OK:
                        SmsGatewayContainer.send("SMS succesfully delivered.");
                        break;
                    case Activity.RESULT_CANCELED:
                        SmsGatewayContainer.send("SMS is failed to get delivered.");
                        break;
                    default:
                        break;
                }
            }
        }, new IntentFilter("SMS_DELIVERED"));

        //Make Intent for Delivery Statuses: Sent & Delivered
        final PendingIntent sendIntent     = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"),0);
        final PendingIntent deliveryIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"),0);

        //Then,
        //Start SMS Gateway Server upon Request
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsServer.setSendIntent(sendIntent);
                smsServer.setDeliveryIntent(deliveryIntent);
                smsServer.start();
                Log.i(SMSServer.class.getName(), " SMS Server started on " + portNo);
            }

        });
        //Stop SMS Gateway Server as Requested
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    smsServer.stop();
                    Log.i(SMSServer.class.getName(), " SMS Server stopped.");
                } catch (IOException e) {
                    Log.e(SMSServer.class.getName(), "IOException|" + e.getMessage(), e);
                } catch (InterruptedException e) {
                    Log.e(SMSServer.class.getName(), "InterruptedException|" + e.getMessage(), e);
                }

            }

        });

        //*** Irrelevant Codes ***//
        //1) Set Action MenuBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //2) Set Floating Compose Message's Action Button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Snackbar Action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });
    }

    //Stopping SMS Gateway Server
    @Override
    protected void onDestroy(){
        try {
            smsServer.stop();
        } catch (IOException e) {
            Log.e(SMSServer.class.getName(), "IOException|" + e.getMessage(), e);
        } catch (InterruptedException e) {
            Log.e(SMSServer.class.getName(), "InterruptedException|" + e.getMessage(), e);
        }

        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_smslist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
