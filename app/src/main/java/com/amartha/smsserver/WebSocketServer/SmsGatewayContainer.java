package com.amartha.smsserver.WebSocketServer;

import org.java_websocket.WebSocket;

import java.util.ArrayList;
import java.util.List;

public class SmsGatewayContainer {

    private static List<WebSocket> sockets = new ArrayList<WebSocket>();

    //Adding WebSocket Client to Container
    public static void add(WebSocket socket){
        sockets.add(socket);
    }
    //Removing WebSocket Client from Container
    public static void remove(WebSocket socket){
        sockets.remove(socket);
    }
    //Sending Responses from Server to Client
    public static void send(String msg){
        for(WebSocket socket:sockets){
            socket.send(msg);
        }
    }
}
