package com.amartha.smsserver.HttpSocketServer;

import org.apache.http.*;
import org.apache.http.impl.*;
import org.apache.http.params.*;
import org.apache.http.protocol.*;

import java.io.*;
import java.net.*;


public class SmsGatewayServer {
    private int port;
    private ServerSocket serverSocket;
    //Reponsible to convert request - response from ServerSocket
    // s.t. inline with HTTP specification
    private HttpService httpService;
    private HttpContext httpContext;

    public SmsGatewayServer(int port){
        this.port   = port;
        httpService = new HttpService(
                new BasicHttpProcessor(),
                new DefaultConnectionReuseStrategy(),
                new DefaultHttpResponseFactory()
        );
        httpContext = new BasicHttpContext();

        //Handler fo HTTP Request
        HttpRequestHandlerRegistry registry = new HttpRequestHandlerRegistry();
        registry.register("*", new SmsGatewayHandler());
        httpService.setHandlerResolver(registry);
    }

    public void start() throws IOException, HttpException {
        //Creating Server's Socket within Port Number
        serverSocket = new ServerSocket(port);
        //Handle incoming HTTP request (blocking) WHILE TRUE
        //Life Cycle: Open, Handle Request, Close.
        while(true){
            Socket socket = serverSocket.accept();
            DefaultHttpServerConnection serverConnection = new DefaultHttpServerConnection();
            serverConnection.bind(socket, new BasicHttpParams());
            httpService.handleRequest(serverConnection, httpContext);
            socket.close();
        }

    }
    public void stop() throws IOException { serverSocket.close(); }
}
