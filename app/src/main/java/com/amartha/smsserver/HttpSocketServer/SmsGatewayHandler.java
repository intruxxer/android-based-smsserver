package com.amartha.smsserver.HttpSocketServer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.util.Log;
import android.telephony.SmsManager;

import java.io.IOException;

public class SmsGatewayHandler implements HttpRequestHandler{
    /*
        {
            "no"  : "085260425714",
            "msg" : "hello world!"
        }
    */
    /* curl -H "Content-Type: application/json" -X POST -d '{"no":"085260425714","msg":"Ini adalah tes."}' http://10.0.1.13:8080 */
    @Override
    public void handle(HttpRequest request, HttpResponse response,
                       HttpContext context) throws HttpException, IOException {
        if(request instanceof HttpEntityEnclosingRequest){
            try{
                HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();

                //Convert String body to JSON
                String message        = EntityUtils.toString(entity);
                JSONObject jsonObject = new JSONObject(message);

                //Extract No & Pesan from JSON Object
                String txtNum  = jsonObject.getString("no");
                String txtMsg  = jsonObject.getString("msg");

                //Send SMS to Designated No.
                SmsManager.getDefault().sendTextMessage(txtNum, null, txtMsg, null, null);

                //Catch response once SUCCESSful
                response.setEntity(new StringEntity("SUCCESS"));
                Log.i(SmsGatewayHandler.class.getName(), "Success sending SMS to " + txtNum);
            }
            catch(Exception e){
                //Catch response if FAILED
                response.setEntity(new StringEntity("FAILED"));
                Log.e(SmsGatewayHandler.class.getName(), e.getMessage(), e);
            }
        }
    }
}
