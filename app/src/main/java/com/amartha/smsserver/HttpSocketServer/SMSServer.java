package com.amartha.smsserver.HttpSocketServer;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.amartha.smsserver.R;

import org.apache.http.HttpException;

import java.io.IOException;

public class SMSServer extends AppCompatActivity {
    private final int portNo = 8080;
    private SmsGatewayServer smsServer;

    //UI References
    private TextView statusTxt;
    private Button   startBtn;
    private Button   stopBtn;
    public SMSServer(){
        smsServer = new SmsGatewayServer(portNo);
    }

    //Starting SMS Gateway Server
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.smslist);
        //Init UI in Main Window
        statusTxt = (TextView) findViewById(R.id.txtStatusServer);
        startBtn  = (Button)   findViewById(R.id.btnStartServer);
        stopBtn   = (Button)   findViewById(R.id.btnStopServer);
        //Start SMS Gateway Server
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            smsServer.start();
                            statusTxt.setText("SMS Server STARTED on port [" + portNo + "]");
                            Log.i(SMSServer.class.getName(), "SMS Server Started in HTTP Promiscuous Mode w/Port: " + portNo);
                        }
                        catch(IOException e){
                            Log.e(SMSServer.class.getName() + " in IO [START]", e.getMessage(), e);
                        }catch(HttpException e){
                            Log.e(SMSServer.class.getName() + " in HTTP", e.getMessage(), e);
                        }
                    }
                }).start();

            }

        });
        //Stop SMS Gateway Server
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            smsServer.stop();
                            statusTxt.setText("SMS Server STOPPED on port [" + portNo + "]");
                            Log.i(SMSServer.class.getName(), "SMS Server Stopped in HTTP Promiscuous Mode w/Port: " + portNo);
                        }
                        catch(IOException e){
                            Log.e(SMSServer.class.getName() + " in IO [STOP]", e.getMessage(), e);
                        }
                    }
                }).start();

            }

        });

        //*** Irrelevant Codes ***//
        //1) Set Action MenuBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //2) Set Floating Compose Message's Action Button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Snackbar Action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });
    }

    //Stopping SMS Gateway Server
    @Override
    protected void onDestroy(){

        try {
            smsServer.stop();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(SMSServer.class.getName(), e.getMessage(), e);
        }
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_smslist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
